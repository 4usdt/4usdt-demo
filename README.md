# 接口签名说明： #
>
API请求在通过internet传输的过程中极有可能被篡改。为了确保请求未被更改需要做签名认证，以校验参数或参数值在传输途中是否发生了更改。  
示例：[4usdt-demo](https://gitlab.com/4usdt/4usdt-demo)  


1. 签名相关术语解释:  
	- 方法请求地址：即访问服务器地址,如 www.4usdt\.com/v1/price。  
	- 签名方法：此处使用 HmacSHA256。  
	- 时间戳（timestamp）：您发出请求的时间 (UTC 时区) (UTC 时区) (UTC 时区) 。如：2019-05-11T16:22:06。在查询请求中包含此值有助于防止第三方截取您的请求。  
	- sign：签名计算得出的值，用于确保签名有效和未被篡改。
	- client_secret: 合作商户密钥，申请接入时获取，用于签名等    
	- 签名公共参数：  
    	- client_id：合作商户唯一标识，申请接入时获取  
    	- timestamp：时间戳，如2019-05-24T16:59:00 
    	- sign_version：签名版本，此处为2  
2. 签名方式：  
	+ sign生成特例说明这里以如下请求为例，来说明sign的生成步骤：
http:\//xxx.xxx\.com/api/v1/price?client\_id=111111&timestamp=2019-05-24T16:59:00&sign\_version=2&currency=USDT&legal\_currency=CNY&sign=aDA3MkRvQm9kZjhoUG0wdU51SW85NmcxQkpYUHVSc3l2ZWFuUjNvZTY2MD0%3D
	+ Step 1. 构造源串  
源串是由2部分内容用“&”拼接起来并进行UrlEncode编码：urlencode(a=x&b=y&...)
将除“sign”外的所有参数按key进行ASCII升序排列，将排序后的参数(key=value)用&拼接起来。
例如：
client\_id=111111&currency=USDT&legal\_currency=CNY&sign\_version=2&timestamp=2019-05-24T16:59:00 
	+ Step 2将上一步生成的字符串进行URL编码。例如：
client\_id%3D111111%26currency%3DUSDT%26legal\_currency%3DCNY%26sign_version%3D2%26timestamp%3D2019-05-24T16%3A59%3A00
	+ Step 3.将上一步生成的字符串使用HMAC-SHA256进行哈希加密  
使用HMAC-SHA256进行哈希运算时使用的密钥为client_secret(在合作商户向4usdt申请时获取)
然后将HMAC-SHA256加密得到的字节码\字符串进行Base64编码，得到的字符串结果如下：  
h072DoBodf8hPm0uNuIo96g1BJXPuRsyveanR3oe660=
	+ Step 4. 将上一步生成的字符串进行再次进行Base64编码，得到字符串结果如下：  
aDA3MkRvQm9kZjhoUG0wdU51SW85NmcxQkpYUHVSc3l2ZWFuUjNvZTY2MD0=
	+ Step 5. 将上一步生成的字符串再次URL编码  
由于生成的签名中可能包含“=”，因此需要再进行一次URL编码，得到的签名值结果如下：  
aDA3MkRvQm9kZjhoUG0wdU51SW85NmcxQkpYUHVSc3l2ZWFuUjNvZTY2MD0%3D
3. 注意事项：
	* 签名验证时，要求对字符串中除了“-”、“_”、“.”之外的所有非字母数字字符都替换成百分号(%)后跟两位十六进制数。
十六进制数中字母必须为大写。
	* 某些系统方法，例如.NET系统方法HttpUtility.UrlEncode会将‘=’编码成‘%3d’，而不是%3D，导致加密签名通不过验证，请开发者注意检查。
Java1.3和早期版本中，调用java.net.URLEncoder下的方法进行URL编码时，某些特殊字符并不会被编码，例如星号(\*)。
由于URL编码规则中规定了星号(\*)必须编码，因此在请求字符串中含星号(\*)的情况下如果使用了上述方法，会导致生成的签名不能通过验证。
因此，如果参数值中含有* ，在使用类java.net.URLEncoder下的方法进行编码后，需开发人员手动将星号字符“*”替换为“%2A”，否则将导致加密签名一直通不过验证，请开发者注意检查。
	* 某些语言的urlencode方法会把“空格”编码为“+”，实际上应该编码为“%20”。这也将生成错误的签名，导致签名通不过验证。 
请开发者注意检查，手动将“+”替换为“%20”。
在PHP中，推荐用rawurlencode方法进行URL编码。
	* **请一定注意**：除了获取时间戳接口/timestamp不需要签名，其他接口都需要签名。对于GET请求，每个方法自带的参数都需要进行签名运算；对于POST请求，每个方法自带的参数不进行签名认证，即POST请求中需要进行签名运算的只有 client\_id、timestamp、sign\_version三个参数，其它参数不参与签名.  
