package com.fourusdt.demo.sign;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

/**
 * 基于jdk8的base64编码
 *
 * @author lvhangfei@yeah.net
 * @date 2018/11/24 17:10
 */
public class Base64Util {

    public static String encode(String source) {
        final byte[] textByte;
        try {
            textByte = source.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            return null;
        }
        return Base64.getEncoder().encodeToString(textByte);
    }

    public static String decode(String target) {
        try {
            return new String(Base64.getDecoder().decode(target), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }


}
