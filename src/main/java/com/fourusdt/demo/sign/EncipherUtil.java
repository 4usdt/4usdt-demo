package com.fourusdt.demo.sign;


import lombok.extern.slf4j.Slf4j;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author lvhangfei@yeah.net
 * @date 2018/11/24 15:58
 */
@Slf4j
public class EncipherUtil {

    public static String urlEncode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    public static String urlDecode(String s) {
        try {
            return URLDecoder.decode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    /**
     * url编码+hmacsha256加密+base64编码+url编码
     * 根据参数获取签名
     *
     * @param paramMap
     * @param key      合作商户在4usdt申请时获取的密钥
     * @return
     */
    public static String getSign(Map<String, Object> paramMap, String key) {
        System.out.println(paramMap);
        Map<String, Object> treeMap = new TreeMap<>(paramMap);
        StringBuffer param = new StringBuffer("");
        treeMap.forEach((k, v) -> param.append(k).append("=").append(v).append("&"));
        String original = param.deleteCharAt(param.length() - 1).toString();
        String urlEncodeString = EncipherUtil.urlEncode(original);
        String hmacsha256Str = HMAC_SHA256.getSign(urlEncodeString, key);
        String base64EncodeStr = Base64Util.encode(hmacsha256Str);
        String target = EncipherUtil.urlEncode(base64EncodeStr);
        System.out.println("签名前字符串:----------" + original);
        System.out.println("urlEncodeString," + urlEncodeString);
        System.out.println("hmacsh256Str," + hmacsha256Str);
        System.out.println("base64EncodeStr," + base64EncodeStr);
        System.out.println("生成签名sign," + target);
        return target;
    }

    public static void main(String[] args) {
        Map<String, Object> map = new HashMap<>();
        map.put("client_id","111111");
        map.put("currency","USDT");
        map.put("legal_currency","CNY");
        map.put("timestamp","2019-05-24T16:59:00");
        map.put("sign_version","2");
        System.out.println(getSign(map,"123"));//aDA3MkRvQm9kZjhoUG0wdU51SW85NmcxQkpYUHVSc3l2ZWFuUjNvZTY2MD0%3D
    }

}
